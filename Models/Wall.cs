﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_NerdySoft.Models
{
    public class Wall
    {
        public Point BeginPoint { get; set; }
        public Point EndPoint { get; set; }

        //координати вектора стіни
        public int X { get; set; }
        public int Y { get; set; }
        public Wall(Point begin, Point end)
        {
            BeginPoint = begin;
            EndPoint = end;

            X = end.X - begin.X;
            Y = end.Y - begin.Y;
        }
    }
}
