﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_NerdySoft.Services;

namespace Task_NerdySoft.Models
{
    public class PointViewModel
    {
        public List<Point> Points { get; set; }
        public string MessageResult { get; set; }
        public bool IsLegal { get; set; }

        public PointViewModel(List<Point> _points, string str, bool isLegal)
        {
            Points = _points;
            MessageResult = str;
            IsLegal = isLegal;
        }
        public PointViewModel() {}
    }
}
