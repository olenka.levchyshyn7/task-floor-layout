﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_NerdySoft.Models;

namespace Task_NerdySoft.Services
{
    public interface IPointInterface
    {
        string GetPoints(List<Point> points, out bool _ok);
    }
}
