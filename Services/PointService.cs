﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_NerdySoft.Models;
using System.Web;



namespace Task_NerdySoft.Services
{
    public class PointService : IPointInterface
    {
        public string GetPoints(List<Point> points, out bool IsLegal)
        {
            if (points == null)
            {
                IsLegal = false;
                return "No data available!";
            }

            if (points.ToArray().Length < 4)
            {
                IsLegal = false;
                return "At least 4 corners must be present!";
            }

            string resultStr = "";
             IsLegal = true;

            if (IsClockwise(points))
                resultStr += "Points provided do go clockwise starting from one of the corners of the room!\n";
            else
            {
                resultStr += "Points provided do not go clockwise starting from one of the corners of the room!\n";
                IsLegal = false;
            }
            if (AreWallsAtRightAngle(points))
                resultStr += "Adjacent Room walls do form right angles!\n";
            else
            {
                resultStr += "Adjacent Room walls do not form right angles!\n";
                IsLegal = false;
            }

            if (IsDiagonalWalls(points))
                resultStr += "There is no diagonal walls!\n";
            else { 
                resultStr += "There is a diagonal wall!\n";
                IsLegal = false;
            }

            if (AreWallsCrossed(new Wall(points[0], points[1]), new Wall(points[0], points[1])))
                resultStr += "Walls do not intersect!\n";
            else { 
                resultStr += "Walls intersect!\n";
                IsLegal = false;
            }

            return resultStr;
        }

        /// <summary>
        /// Assumptions 1: Points provided go clockwise starting from one of the corners of the room.
        /// </summary>
        /// <param name="points"></param>
        /// <returns> true if yes, else - false </returns>
        public static bool IsClockwise(List<Point> points)
        {
            int sum = 0;
            int l = points.ToArray().Length;
            for (int i = 0; i < l - 1; i++)
            {
                sum += (points[i + 1].X - points[i].X) * (points[i + 1].Y + points[i].Y);
            }
            if (sum > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Assumption 2: Adjacent Room walls form right angles
        /// </summary>
        /// <param name="w1"> First wall </param>
        /// <param name="w2"> Second wall </param>
        /// <returns> true if form, else - false </returns>
        public static bool IsAtRigthAngle(Wall w1, Wall w2)
        {
            bool _isAtRightAngle = ((w1.X * w2.X + w1.Y * w2.Y) == 0) ? true : false;             
            //скалярний добуток перпендикулярних векторів == 0
            if (_isAtRightAngle)
                return true;
            else
                return false;
        }

        public static List<Wall> GetListWall(List<Point> points)
        {
            //ініціалізація
            List<Wall> WallsList = new List<Wall>();
            int i = 0;
            for (; i < points.ToArray().Length - 1; ++i)
            {
                Wall w = new Wall(points[i], points[i + 1]);
                WallsList.Add(w);
            }
            WallsList.Add(new Wall(points[0], points[i]));
            return WallsList;
        }

        public static bool AreWallsAtRightAngle(List<Point> points)
        {
            //ініціалізація
            List<Wall> WallsList = GetListWall(points);

            //перевірка, чи суміжна стіни утворюють прямий кут   
            int i = 0;
            for (; i < WallsList.ToArray().Length - 1; ++i)
            {
                if (!IsAtRigthAngle(WallsList[i], WallsList[i + 1]))
                {
                    return false;
                }
            }
            //перевірка, чи остання та перша стіна утворюють прямий кут
            if (!IsAtRigthAngle(WallsList[i], WallsList[0]))
                return false;
            return true;
        }

        public static bool IsDiagonalWalls(List<Point> points)
        {
            //ініціалізація
            List<Wall> WallsList = GetListWall(points);

            //перевірка, чи нема діагональних стін
            //перевіряється, чи стіна та одиничний вектор утворюють прямий кут
            //якщо ні, то є хоча б 1 діагональна стіна
            Wall w0 = new Wall(new Point(0, 0), new Point(1, 0));
            int i = 0;
            for (; i < WallsList.ToArray().Length; ++i)
            {
                bool _isCollinear = (WallsList[i].Y == 0) ? true : false; // тоді вектор буде колінеарний до нульового
                if (!IsAtRigthAngle(WallsList[i], w0) && !_isCollinear)
                    return false;
            }
            return true;
        }

        public static bool Between(int num, int a, int b)
        {
            if ((num <= a && num >= b) || (num >= a && num <= b))
                return true;
            else
                return false;
        }
        public static bool AreWallsCrossed(Wall w1, Wall w2)
        {
            //якщо координата х одної з прямих лежить між іксами другої та 
            //у другої лежить між ігриками першої
            //або навпаки, то стіни перетинаються
            if ((Between(w1.BeginPoint.X, w2.BeginPoint.X, w2.EndPoint.X)
                && Between(w2.BeginPoint.Y, w1.BeginPoint.Y, w1.EndPoint.Y))
                || (Between(w2.BeginPoint.X, w1.BeginPoint.X, w1.EndPoint.X)
                && Between(w1.BeginPoint.Y, w2.BeginPoint.Y, w2.EndPoint.Y)))
                return true;
            else
                return false;
        }

    }
}
