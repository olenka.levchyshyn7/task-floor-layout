﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Core;
using Microsoft.AspNetCore.Routing;
using Task_NerdySoft.Models;
using Task_NerdySoft.Services;

namespace Task_NerdySoft.Controllers
{
    //[PassParametersDuringRedirect]
    public class PointController : Controller
    {
        private readonly IPointInterface _pointService;

        public PointController(IPointInterface pointService)
        {
            _pointService = pointService;
        }

        public ActionResult Index(PointViewModel model)
        {
            return View(model);
        }

        [HttpPost]
        public IActionResult Calculate([FromBody] List<Point> points)
        {
            bool isLegal;
            var result = _pointService.GetPoints(points,out isLegal);
            var model = new PointViewModel(points, result, isLegal);
            return Json(Url.Action("Index", "Point", model));
        }
    }
} 
