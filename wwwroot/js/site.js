﻿function NewPoint() {
    var division = document.createElement('DIV');
    division.innerHTML = NewTextBox("");
    document.getElementById("firstdiv").appendChild(division);
}
function NewTextBox(value) {
    return '<div><p>Enter x coordinate: </p><input type = "number" name = "X" required/><br /><p>Enter y coordinate: </p><input type="number" name="Y" required/><br><br/><input type="submit" value="Remove" onclick="ReTextBox(this)" /><br><br/><div>';
}
function ReTextBox(div) {
    document.getElementById("firstdiv").removeChild(div.parentNode.parentNode);
}

$("body").on("click", "#btnSubmit", function (event) {
    var elems = document.getElementById('pointsForm').elements;
    var points = new Array();
    var point = { x: 0, y: 0 };

    var elem = new Array();
    for (var i = 0; i < elems.length; i++) {
        if (elems[i].type == "number") {
            if (elems[i].value == null || elems[i].value == "") {
                alert("Fill all coordinates!");
                return;
            }
            elem.push(elems[i]);
        }
    }
    for (var i = 0; i < elem.length; i += 2) {
        point.x = +elem[i].value;
        point.y = +elem[i + 1].value;
        points.push(point);
        point = { x: 0, y: 0 };
    }
    if (points.length >= 4) {
        // Send the JSON array to Controller using AJAX.
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: "/Point/Calculate",
            data: JSON.stringify(points),
            contentType: "application/json",
        }).then(function (data) {
            console.info(data); 
            window.location.href = data;
        }
        );
    }
    else {
        alert("At least 4 points should be added!");
    }
})